## Lab 1 Create and Run Dockerized Application

> **IMPORTANT** Even though the steps are unnumbered, perform all steps in the order shown. Click the checkboxes as you complete steps to avoid losing your place.

### Prerequisities

* A text or code editor.

* A confirmed user id on Git**Lab** (not GitHub). 

  * [Create a GitLab User Id by clicking here](https://gitlab.com/users/sign_up). Don’t forget to confirm using the link in the registration email that is sent. ]

* Docker Desktop Installed on your development machine: https://www.docker.com/products/docker-desktop

  * [Guide for Installing Docker Desktop on Windows](https://andrewlock.net/installing-docker-desktop-for-windows/)

* Git installed on your your development machine.

  > Be sure that you copy and paste these as a single line into a command prompt / terminal window
  
  * Oneliner command for Windows (also installs Chocolatey package manager)
    ```powershell
    [Net.ServicePointManager]::SecurityProtocol = [Net.SecurityProtocolType]::Tls12,[Net.SecurityProtocolType]::Tls11; 
    If (!(Test-Path env:chocolateyinstall)) {iwr https://chocolatey.org/install.ps1 -UseBasicParsing | iex} ; cinst -y git
    ```
  
  * Oneliner command for Mac (also installs Brew package manager)
    ```shell
    if [[ -z "(command -v brew)" ]]; then /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"; fi; if [[ -z "(command -v git)" ]]; then brew install git; fi
    ```

### 1. Build and Run the Docker Container

* [ ] In a command prompt on your development machine, type or paste the following command: `git clone https://gitlab.com/whatandwhyofdevops1/microwebserver.git`

* [ ] Change to the repo folder using `cd microwebserver`

* [ ] Build the Docker file with `docker build -t myserver .` **Note:** The trailing dot (“`.`”) is required.

  > **Note**: `-t myserver` simple gives the local container a name.

* [ ] When the build has completed, launch the container with: `docker run -p 5000:5000 -it myserver`

* [ ] In a web browser on your machine, visit: `http://localhost:5000` 

  > **Note**: In many browsers you must type out the URL to avoid the default SSL prefix ending in “s” => “http**`S`**”

* [ ] If you do not get a response from the server in the browser, try this alternative way of launching the container:

  * [ ] Terminate the currently running container
  * [ ] Relaunch the container with: `docker run -it --net=host myserver`
  * [ ] In a web browser on your machine, visit: `http://localhost:5000`

> Note that whether or not your machine has python installed, the webserver will work because python is installed in the container.

### 2. Make a change to the Code

- [ ] Click terminal where the docker container is running and press CTRL-C / Control-C to terminate the container.
- [ ] Use your coding editor to edit `src/microwebserver.py`
- [ ] Find the text `lightgreen` and change it to `lightblue`
- [ ] Save the file and exit your text editor.
- [ ] If you changed directories to edit, change back to the directory where the `Dockerfile` is.
- [ ] Rebuild the Docker file with `docker build -t myserver .`
- [ ] When the build has completed, launch the container with: `docker run -p 5000:5000 --rm myserver`
- [ ] In a web browser on your machine, visit: `http://localhost:5000` (or refresh the page if it is already open)
