## Lab 2 See CI In Action

> **IMPORTANT** Even though the steps are unnumbered, perform all steps in the order shown. Click the checkboxes as you complete steps to avoid losing your place.

### Prerequisities

* A confirmed user id on Git**Lab** (not GitHub). 

  * [Create a GitLab User Id by clicking here](https://gitlab.com/users/sign_up). Don’t forget to confirm using the link in the registration email that is sent. 


### 1. Triggering GitLab Auto DevOps CI

* [ ] In your web browser, type or paste the following url: `https://gitlab.com/whatandwhyofdevops1/microwebserver`

* [ ] On the left navigation bar *click* **Issues => List** (You may have to hover the icons to find the one titled “Issues”)

  > **Watch for:** While it is an extremely common work flow for developers to create branches locally and only push to the shared SCM system after signficant work has been completed, GitLab encourages development transparency and Continuous Integration through workflow where branches and merge requests are created before starting coding work. This ensures issues are links to merge requests and branches properly and everyone can see progress as soon as work actually starts.

* [ ] In the upper left, *click* **New issue**

* [ ] In the New issue page, for Title, *Type* **YOURGLUSERNAME App Updates** (where YOURGLUSERNAME = your gitlab username)

* [ ] *Click* **Assign to me**

* [ ] In the bottom left *Click* **Create issue**

* [ ] In the bottom right of the issue *Click* **Create merge request**

* [ ] *Click* **Assign to me**

* [ ] In the bottom left of the Merge Request *Click* **Create merge request**

* [ ] Near the top of the Merge Request *click* the **Pipelines** tab

  > Note: When you create a new branch it makes a change to the repository, which triggers GitLab CI. GitLab CI is busy creating an isolated kubernetes environment for the code in your branch.

* [ ] At the left, under Status, right click the icon and select Open Link in New Window

### 2. Explore Pipelines and Jobs

- [ ] In the new window, click into and back out of various jobs to view the job logs.

  > The job logs indicate what has been done and are commonly used when searching for root cause of CI automation problems.

- [ ] After your curiorsity is satisfied, close the browser window.

- [ ] Back in the Merge Request page, near the top, *click* the **Overview** tab.

  > If you closed the merge request window, in the project, on the left locate and *click* **Merge requests** (it will have your GitLab user name in the title just like your issue)

### 3. Update The Application

> In this section you will make some visible 

- [ ] Near the top click “Open in Web IDE”

  > Note: GitLab has a fairly substantial editor built in. For an even fuller developer experience GitPod can be used to create a complete editor + prototyping environment.

- [ ] In the editor *locate the text* **lightgreen** and *update it to* **lightblue** (or any color from these [HTML Color Names](https://htmlcolorcodes.com/color-names/))

- [ ] In the editor *locate the text* **DEFAULT** and *update it to* **Your Name**’s

- [ ] At the bottom left, click Commit…

- [ ] At the *bottom left*, in *Commit Message* *Type* **Updated color and name**

- [ ] *Click* **Commit**

- [ ]  In the status bar at the bottom of the page, *after the text* *Merge request*, *click* the **MR number** which is the number that is proceeded by the exclamation character (“``!``”)

- [ ] In the second block on the page you should see the Pipeline status details. 

  > You can examine other parts of the MR while the Pipeline completes.

- [ ] Once the pipeline completes, refresh the page.

- [ ] Under the pipeline status, on the right side *click* **View app**

  > Note: This is similar to what a previous lab did on the local machine. However, since this environment is hosted on shared infrastructure, anyone could do manual testing and we can turn a host of automated

- [ ] The address of your website is the same, but the background color and text should reflect the code changes that you made.

- [ ] Share your review app URL with the class through slack or zoom chat.

- [ ] Click someone else’s URL to see that they have a different URL and different contents in their isolated review app.

  > Isolated environments for each feature branch mean that security, quality and license findings are specific to each developers code changes. This means that developers immediately become aware of security and other code problems they have accidentally introduced. This allows these problems to be dealt with before they become hard to find and hard to fix.
  
- [ ] On the left navigation bar, locate and *Click* **Deployments => Environments**

- [ ]  To the right of *production* *Click* **Open**

- [ ] The background should be light green and the sentence should be `Hello world from DEFAULT superlight Python web server.`

### 4. Check Out Security Vulnerabilities

- [ ] Back in your *Merge request*, next to the line starting with *Security scanning detected*…, *Click* **Expand**

- [ ] Under Dast, there should be at least one security finding, Click the finding title.

- [ ] Examine the vulnerability details.

- [ ] Near the *bottom right* *Click* **Dismiss vulnerability**

  > Note: GitLab issues can be created with all the vulnerability information for handling later or for collaborating about how to remediate it. It is easier to resolve vulnerabilities immediately when you likely just introduced them as you can track down new versions of dependencies or better code to close the vulnerability.
