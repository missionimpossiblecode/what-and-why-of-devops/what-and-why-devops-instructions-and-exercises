## Lab 3 CI Catches Problems Early

> **IMPORTANT** Even though the steps are unnumbered, perform all steps in the order shown. Click the checkboxes as you complete steps to avoid losing your place.

In this lab, we will go over how Vulnerabilities can be viewed as well as the information and actions available to a user. We are going to add some vulnerable code to a feature branch and then the scanners will run and display the found vulnerabilities.

## Step 1: Adding Vulnerable Code

- [ ] In your web browser, type or paste the following url: `https://gitlab.com/whatandwhyofdevops1/simply-simple-notes`

- [ ] On the left navigation bar *click* **Issues => List** (You may have to hover the icons to find the one titled “Issues”)

- [ ] In the upper left, *click* **New issue**

- [ ] In the New issue page, for Title, *Type* **YOURGLUSERNAME Add Cool Code** (where YOURGLUSERNAME = your gitlab username) 

- [ ] *Click* **Assign to me**

- [ ] In the bottom left *Click* **Create issue**

- [ ] In the bottom right of the issue *Click* **Create merge request**

- [ ] *Click* **Assign to me**

- [ ] In the bottom left of the Merge Request *Click* **Create merge request**
- [ ] Near the top *Click* **Open in Web IDE**
- [ ] Open `notes/db.py` and add the following under `conn = sqlite3.connect(name)`. (About line 11) This is done to give the database file global permissions, which is a security issue

```python
os.chmod(name, 777)
```

- [ ] Open `notes/db.py` and add the following under `conn = sqlite3.connect(name)` and add to following to the end of the file.

```
aws_key_id = "AKIAIOSFODNN7EXAMPLE"
aws_key_secret = "wJalrXUtnFEMI/K7MDENG/bPxRfiCYEXAMPLEKEY"
```

- [ ] Open `notes/routes.py` and add to the end of the file. This will add a new route that can be accessed at the `/get-with-vuln` URI path that allows us to test DAST in this lab scenario

```python
@note.route('/get-with-vuln', methods=['GET'])
def get_note_with_vulnerability():
    id = request.args.get('id')
    conn = db.create_connection()

    with conn:
        try:
            return str(db.select_note_by_id(conn, id))
        except Exception as e:
            return "Failed to delete Note: %s" % e
```

- [ ] Create a file in `chart/templates` (also create these directory levels) called `vulns.yaml` and add the following:

```yaml
apiVersion: v1
kind: Pod
metadata:
    name: kubesec-demo
spec:
    containers:
    - name: kubesec-demo
      image: gcr.io/google-samples/node-hello:1.0
      securityContext:
        readOnlyRootFilesystem: true
```

- [ ] Open `requirements.txt` and change the content to the following (you can do a full replace to avoid syntax problem if you wish, but note that we are only adding two packages and version pegging the Flask package):

```text
Flask==0.12.2
django==2.0.0
flask_wtf
wtforms
flask-bootstrap
pysqlite3
dubbo-client
```

- [ ] Open `Dockerfile` and change the alpine version to the following:

```text
FROM python:alpine3.7
```

- [ ] *Click* **Commit…**
- [ ] At the *bottom left*, in *Commit Message* *Type* **Updated with cool code**
- [ ] *Click* **Commit**

- [ ] In the status bar at the bottom of the page, *after the text* *Merge request*, *click* the **MR number** which is the number that is proceeded by the exclamation character (“``!``”)
- [ ] Create the Merge Request and press **Submit merge request**

## Step 2: Viewing Vulnerable Code

Now we can view the vulnerabilities after the pipeline started above
has completely run. Let's dig into the vulnerabilities and perform some actions on them.

- [ ] Go to the merge request created in Step one.

- [ ] Within the merge request, press **Expand** on the Security Scans

> **Note** It will take a few mins for the security scans to complete.

- [ ] Click on the **Chmod setting a permissive mask 0o1411 on file (name)** vulnerability and you'll get a popup

- [ ] Dismiss the Vulnerability by clicking **Dismiss vulnerability**

> **Note** You can now see a label next to the dismissed vulnerability

This allows AppSec teams to see what developers are dismissing as well as why. If this MR were to be merged, then the vulnerability will automatically be tagged as dismissed in the vulnerability report.

- [ ] Click on the same vulnerability

- [ ] Click on **Create issue**

Now let's go back to the Merge Request by pressing the back button on your browser.

## Step 3: Viewing Denied Licenses

Within the same MR view, we can see the licenses that were detected. You'll be able to see which licenses are approved and denied according to the policy we set in an earlier lab.

- [ ] Within the merge request expand the **license** section

- [ ] See that the **Apache License 2.0** has been denied

### Optional Step 4: Remediating Security Vulnerabilities To Fix The Build

> Watch For: Resolving vulnerabilities causes the build to succeed and be deployed.

- [ ] Use the WEBIDE to reverse all above changes. This will be fairly easy because the IDE will be in a diff view which shows all your edits, making them easy to reverse.
- [ ] After committing the changes in the Web IDE a new build will kick off.
- [ ] Examine the Merge Request (MR) Overview tab to see that the security findings are resolved (should say “Security scanning detected **no** vulnerabilities.”)
- [ ] Examine the MR Pipeline tab to see that the deployment progresses.
